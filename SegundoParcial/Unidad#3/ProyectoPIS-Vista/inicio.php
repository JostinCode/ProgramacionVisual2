<?php require_once 'includes/cabeceraPrincipal.php'; ?>
<?php require_once 'includes/logeo.php'; ?>
<?php require_once 'includes/anuncio.php'; ?>
<?php require_once 'includes/cabeceraMenu.php'; ?>

<!-- CAJA PRINCIPAL -->
<div id="principal">
    <h1 align="center">SOBRE NOSOTROS</h1> <br>
    <div id="informacionUno">
        <h2 align="center"> ¿QUIÉNES SOMOS? </h2> <br>
        Company Shop, abrió por primera vez en agosto del 2020
        en Daule.
        <br> <br>
        Company Shop ofrecemos gran variedad de surtido que incluye
        los artículos más importantes y necesarios de tu día a día. 
        Además, ofrecemos muchos productos interesantes de 
        excelente calidad a los precios más bajo del mercado.
        <br> <br>
        <h2 align="center"> GARANTIZAMOS </h2> <br>
        Muy buena calidad, la cual revisamos estrictamente por 
        laboratorios independientes.
        <br> <br>
        <h2 align="center"> AHORRAMOS </h2> <br>
        Donde quiera que podamos evitamos cualquier costo, el cual 
        potencialmente incrementa el precio de nuestra publicidad del
        producto. Nuestra empresa se ubica en el cantón Daule, aspiramos 
        hacer unas de las más grandes empresa de cantón, en nuestro 
        surtido llevamos los artículos más importantes de consumo diario, 
        semanal y mensual, la presentación de los productos es muy 
        simples, nuestros gastos de marketing son muy bajos.
    </div>
    <div id="imagenUno">
        <img src="./assets/img/imagenPersona1.png" width="460px" height="530px" />
    </div>
    <div id="imagenDos">
        <img src="./assets/img/imagenPersona2.png" width="480px" height="535px" />
    </div>
    <div id="informacionDos">
        <h2 align="center"> ENTREGAMOS </h2> <br>
        Muy buena calidad y las mejores fechas de vencimiento.
        <br> <br>
        <h2 align="center"> VENDEMOS </h2> <br>
        Calidad comparable siempre al precio más bajo del 
        mercado.
        <br> <br>
        <h2 align="center"> ESTAMOS ORGULLOSOS </h2> <br>
        De nuestros empleados amables y eficientes.
        <br> <br>
        <h2 align="center"> GARANTÍA </h2> <br>
        Ofrecemos a nuestros clientes el beneficio de una 
        política de devolución incondicional de productos que 
        no alcancen sus expectativas. 
        <br> <br>
        <h2 align="center"> PROMETEMOS </h2> <br>
        Atendernos a nuestros principios para su beneficio, 
        ¡todos los días!
    </div>
</div>
<!-- FIN PRINCIPAL -->

<?php require_once 'includes/pie.php'; ?>
