<?php require_once 'includes/conexion.php'; ?>
<?php require_once 'includes/helpers.php'; ?>
<?php
    $categoria_actual = conseguirCategoria($db, $_GET['id']);

    if (!isset($categoria_actual['id'])) {
        header("Location: inicio.php");
    }
?>
<?php require_once 'includes/cabeceraPrincipal.php'; ?>
<?php require_once 'includes/busqueda.php'; ?>
<?php require_once 'includes/cabeceraMenu.php'; ?>

<!-- CAJA PRINCIPAL -->
<div id="principal">

    <h1 align="center">Productos de <?=$categoria_actual['nombre']?> </h1> <br>

    <?php
    $productos = conseguirProductos($db, null, $_GET['id']);

    if ($productos && mysqli_num_rows($productos) >= 1):
        while ($producto = mysqli_fetch_assoc($productos)):
    ?>
            <div class="tablas">
                <div id="tabla1">
                    <table border="" align="left">
                        <tr> <td> <img src="data:image/png;base64, <?php echo base64_encode($producto['imagen']) ?>" width="290px" height="330px"/> </td> </tr>
                    </table>
                </div>
                
                <?php if ($categoria_actual['id'] == 1) { ?>
                    <div id="tabla2">
                        <table border="" align="left">
                            <tr>
                                <td colspan="2" align="left">
                                    <?php if (strlen($producto['descripcion']) <= 30):
                                        echo $producto['descripcion'];
                                    else:
                                        echo substr($producto['descripcion'], 0, 30)."<br>".substr($producto['descripcion'], 31);
                                    endif; ?>
                                </td>
                            </tr>
                            <tr> <td class="colorRojo" align="center">           Oferta           </td> <td class="colorRojo" align="center"> <?= $producto['cuotas'] ?> Cuotas </td> </tr>
                            <tr> <td class="big" align="center">    <?= $producto['oferta'] ?>    </td> <td class="big" align="center">     <?= $producto['valorCuotas'] ?>     </td> </tr>
                            <tr> <td class="colorRojo" align="center">            PVP             </td> <td class="colorRojo" align="center">            Precio final           </td> </tr>
                            <tr> <td class="colorRojo" align="center">  <?= $producto['PVP'] ?>   </td> <td class="colorRojo" align="center">  <?= $producto['precioFinal'] ?>  </td> </tr>
                            <tr> <td colspan="2" align="left">    VER PRECIO:   </td> </tr>
                            <tr>
                                <td colspan="2">
                                    <?php if (strlen($producto['tipoPago']) <= 31):
                                        echo $producto['tipoPago'];
                                    else:
                                        echo substr($producto['tipoPago'], 0, 31)."<br>".substr($producto['tipoPago'], 31);
                                    endif; ?>
                                </td>
                            </tr>
                        </table>
                    </div>
                <?php } else { ?>
                    <div id="tabla2">
                        <table border="" align="left">
                            <tr>
                                <td colspan="2"> <?= $producto['nombre'] ?> </td>
                            </tr>
                            <tr>
                                <td colspan="2" align="left">
                                    <?php if (strlen($producto['descripcion']) <= 75):
                                        echo $producto['descripcion'];
                                    else:
                                        echo substr($producto['descripcion'], 0, 75)."<br>".substr($producto['descripcion'], 75);
                                    endif; ?>
                                </td>
                            </tr>
                            <tr> <td class="colorRojo" align="center">        PRECIO FINAL        </td> <td class="big" align="center"> <?= $producto['precioFinal'] ?> </td> </tr>
                            <tr> <td colspan="2" align="left">    VER PRECIO:   </td> </tr>
                            <tr>
                                <td colspan="2"> <?php echo $producto['tipoPago']; ?> </td>
                            </tr>
                        </table>
                    </div>
                <?php } ?>
                
            </div>
            <?php
        endwhile;
    else:
            ?>
        <div class="alerta">No hay productos en esta categoría</div>
    <?php endif; ?>
</div>
<!--FIN PRINCIPAL-->

<?php require_once 'includes/pie.php'; ?>
