<?php
	if(!isset($_POST['busqueda'])){
		header("Location: index.php");
	}
?>
<?php require_once 'includes/cabeceraPrincipal.php'; ?>
<?php require_once 'includes/busqueda.php'; ?>
<?php require_once 'includes/cabeceraMenu.php'; ?>
		
<!-- CAJA PRINCIPAL -->
<div id="principal">

	<h1>Busqueda de: <?=$_POST['busqueda']?></h1> <br> <br>
	
	<?php 
		$productos = conseguirProductos($db, null, null, $_POST['busqueda']);

		if($productos && mysqli_num_rows($productos) >= 1):
			while($producto = mysqli_fetch_assoc($productos)):
	?>
				<article class="entrada">
					<a href="<?php//entrada?id=<?=$producto['id_producto']?>">
						<h2><?=$producto['nombre']?></h2>
						<span class="fecha"><?=$producto['categoria'].' | '.$producto['tipoPago']?></span>
						<p>
							<?=substr($producto['descripcion'], 0, 200)."..."?>
						</p>
					</a>
				</article>
	<?php
			endwhile;
		else:
	?>
		<div class="alerta">No hay productos de esta categoría referente a su búsqueda</div>
	<?php endif; ?>
</div>
<!--FIN PRINCIPAL-->
			
<?php require_once 'includes/pie.php'; ?>