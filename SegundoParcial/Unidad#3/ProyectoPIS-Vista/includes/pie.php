
			<div class="clearfix"></div>
		</div>
                <!-- FIN DEL CONTENEDOR -->
                
                <div id="lineaDivision"></div>
                
		<!-- PIE DE PÁGINA -->
		<footer id="pie">
                    <div id="contactosRedes">
                        <div id="contactos">
                            <p> <strong>CONTÁCTANOS POR:</strong> </p> <br>
                            <strong>Correo:</strong>
                            <a href="https://companyshoptds@gmail.com" target="_blank">companyshoptds@gmail.com</a> <br> <br>
                            <strong>Teléfono:</strong> 0963699961 - 0961239022 <br> <br>
                        </div>
                        <div id="redesSociales">
                            <p> <strong>REDES SOCIALES</strong> </p> <br>
                            <a href="https://www.facebook.com" target="_blank">
                                <img src="./assets/img/facebookIcono.png" width="50px" height="50px" />
                            </a> &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp;
                            <a href="https://www.instagram.com/" target="_blank">
                                <img src="./assets/img/instagramIcono.png" width="52px" height="52px" />
                            </a> &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp;
                            <a href="https://web.whatsapp.com" target="_blank">
                                <img src="./assets/img/whatsappIcono.png" width="51px" height="51px" />
                            </a>
                        </div>
                        <div id="derechos">
                            &copy; 2021 TIENDAS INDUSTRIALES ASOCIADAS (CS). TODOS LOS DERECHOS RESERVADOS.
                        </div>
                    </div>
                    <div id="companyLogo">
                        <img src="./assets/img/logoPrincipal.png" width="215px" height="117px"/>
                    </div>
		</footer>
		
	</body>
</html>
