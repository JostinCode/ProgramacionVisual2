<?php
//Encriptación de contraseñas
echo "<h1> Cifrado de contraseñas </h1>";

echo "<h2> Hasheando la contraseña: </h2>";
$contraseña = "Hola";
echo "Valor de la contraseña: " . $contraseña . "<br>";

$contraseñaCifrada = password_hash($contraseña, PASSWORD_DEFAULT);
echo "Valor del hash de la contraseña: " . $contraseñaCifrada . "<br>"; //Presentando el valor de hash
//Verificación del hash de la contraseña:
echo "<h2> Verificando la contraseña: </h2>";
?>

<form action="" method="POST">
    <input type="text" name="ingreso" placeholder="Ingrese la contraseña" /> &nbsp; <input type="submit" name="envio" value="Verificar" />
</form>

<?php
if (isset($_POST['envio'])) {
    
    if (isset($_POST['ingreso'])) {
        $contraseñaVerificada = password_verify($_POST['ingreso'], $contraseñaCifrada);
        if ($contraseñaVerificada):
            echo "La contraseña es correcta";
        else:
            echo "La contraseña es incorrecta";
        endif;
    } else {
        echo "¡Error de verificación! No se ha ingresado la contraseña";
    }
    
}

