<?php
/* Archivo principal para la ejecución del programa */
//Logueo de usuario: 
session_start(); //Inicio de la sesión
?>

<html>
    <head>
        <title>Log-in de usuario</title>
        <link rel="stylesheet" type="text/css" href="Assets/Styles.css" />
    </head>
    <body>
        <header class="cabecera">

<?php if (!isset($_SESSION['usuario'])) { //Verificamos si hay sesión ?>
            
            <!-- Botónes para registro e inicio de sesión -->
            <form action="" method="POST" enctype="multipart/form-data">
                <input type="submit" name="registrese" value="Regístrese" /> &nbsp; &nbsp; &nbsp; &nbsp;
                <input type="submit" name="inicieSesion" value="Inicie sesión" />
            </form>
            
        </header>
        <div id="Log_in">
            <img src="./Assets/Usuario.png" width="300"/>
            <?php if (isset($_POST['registrese'])) { ?>
                <!-- Formulario para el registro de datos -->
                <br> <br> <br> <h1 align="center"> Regístrese en la base de datos </h1> <br>
                <form action="Registro.php" method="POST" enctype="multipart/form-data">
                    <table align="center" border="0px">
                        <tr>
                            <td class="Espacio">Nombres:</td> <td><input type="text" name="nombres" placeholder="Ingrese sus dos nombres" required /></td>
                        </tr>
                        <tr>
                            <td class="Espacio">Apellidos:</td> <td><input type="text" name="apellidos" placeholder="Ingrese sus dos apellidos" required /></td>
                        </tr>
                        <tr>
                            <td class="Espacio">Edad:</td> <td><input type="number" name="edad" placeholder="Ingrese su edad" /></td>
                        </tr>
                        <tr>
                            <td class="Espacio">Fecha de nacimiento:</td> <td><input type="date" name="fechaNacimiento" placeholder="" /></td>
                        </tr>
                        <tr>
                            <td class="Espacio">Nombre de usuario:</td> <td><input type="text" name="nombreUsuario" placeholder="Ingrese su usuario" /></td>
                        </tr>
                        <tr>
                            <td class="Espacio">Password o contraseña: &nbsp; </td> <td><input type="password" name="contraseña" placeholder="Ingrese su contraseña" /></td>
                        </tr>
                        <tr>
                            <td class="Espacio">Correro electrónico:</td> <td><input type="email" name="correo" placeholder="Ingrese su correo" /></td>
                        </tr>
                        <tr> <td class="EspacioBoton" colspan="2" align="center"> <input id="BotonRegistrar" type="submit" name="registrar" value="Registrar" /> </td> </tr>
                    </table>
                </form>
            <?php } else if (isset($_POST['inicieSesion'])) { ?>
                <!-- Formulario para el ingreso al sistema -->
                <br> <br> <br> <h1 align="center"> Inicie sesión en el sistema </h1> <br>
                <form action="Ingreso.php" method="POST" enctype="multipart/form-data">
                    <table>
                        <tr>
                            <td class="Espacio">Nombre de usuario:</td>
                            <td><abbr title="Digite su nombre de usuario"> <input type="text" name="usuario" placeholder="Usuario" /> </abbr></td>
                            <td align="center"> &nbsp; <img src="./Assets/IconoUser.png" align="center" width="26" /></td>
                        </tr>
                        <tr>
                            <td class="Espacio">Password o contraseña: &nbsp; </td>
                            <td><abbr title="Digite su contraseña"> <input type="password" name="contraseña" placeholder="Contraseña" /> </abbr></td>
                            <td align="center"> &nbsp; <img src="./Assets/IconoPassword.png" align="center" width="23" /></td>
                        </tr>
                        <tr>
                            <td class="Espacio">Correro electrónico:</td>
                            <td><abbr title="Digite su correro o Email"> <input type="email" name="correo" placeholder="Correo" /> </abbr></td>
                            <td align="center"> &nbsp; <img src="./Assets/IconoEmail.png" align="center" width="27" /></td>
                        </tr>
                        <tr>
                            <td class="EspacioBoton" colspan="3" align="center"><input id="BotonIniciarSesion" type="submit" name="iniciarSesion" value="Iniciar sesión" /></td>
                        </tr>
                    </table>
                </form>
            <?php } ?>
            
<?php } else { ?>
            
            <!-- Formulario para finalizar la sesión activa -->
            <h1> Sesión activa </h1> <br>
            Bienvenido <?= $_SESSION['usuario']['nombres'] ?> <?= $_SESSION['usuario']['apellidos'] ?>,
            o también conocido como <?php echo $_SESSION['usuario']['nombreUsuario'] ?>.
            <br> <br>
            <form action="Cerrar.php" method="POST" enctype="multipart/form-data">
                <input type="submit" name="cerrar" value="Cerrar sesión" />
            </form>
        </header>
        <div id="Log_in">
            <img src="./Assets/Bienvenido.jpg" width="100%" />
<?php } ?>

        </div>
        <footer id="pie">
            <h3>Programa para el registro de datos y login de usuario</h3>
        </footer>
    </body>
</html>

