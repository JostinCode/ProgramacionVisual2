
<html>
    <head>
        <title>Log-in de usuario</title>
        <link rel="stylesheet" type="text/css" href="Assets/Styles.css" />
    </head>
    <body>
        <header class="cabecera">
            
            <?php
            session_start(); //Inicio de la sesión
            if (isset($_POST['iniciarSesion'])) { //Si existe la variable super global $_POST, se ejecutará lo siguiente:
    
                include './Conectar.php'; //Conexión a la base de datos
    
                if (!empty($_POST['usuario']) && !empty($_POST['contraseña']) && !empty($_POST['correo'])) {
                    /* Variables del formulario: */
                    $usuario = $_POST['usuario']; //A $usuario le asignamos el valor que contiene la variable usuario del formulario
                    $contraseña = $_POST['contraseña']; //A $contraseña le asignamos el valor que contiene la variable contraseña del formulario
                    $correo = trim($_POST['correo']); //A $correo le asignamos el valor que contiene la variable correo del formulario
        
                    $sql = "SELECT * FROM usuario WHERE correo = '$correo';"; //Consulta
                    $resultado = mysqli_query($conexion, $sql); //Ejecutamos e igualamos la consulta
        
                    if ($resultado && mysqli_num_rows($resultado) == 1) {
            
                        $usuarioLoguado = mysqli_fetch_assoc($resultado);
                        /* Desencriptamos la contraseña */
                        $contraseñaVerificada = password_verify($contraseña, $usuarioLoguado['contraseña']); //Verificamos el password
            
                        if ($contraseñaVerificada) : //Si la verificación es correcta y se ejecutó correctamente, se realizará lo siguiente:
                            if ($usuario == $usuarioLoguado['nombreUsuario']) :
                                $_SESSION['usuario'] = $usuarioLoguado; //Asignamos la variable de sesión para el usuario
                                echo "<h1> Usuario logueado correctamente </h1>";
                                header('Refresh:5 Loguear.php');
                            else: //Si está mal la verificación, se hará lo siguiente:
                                $_SESSION['errorLogin'] = "Error de login"; //Asignamos la variable de sesión para el error (usuario incorrecto)
                                echo "<h1> Usuario incorrecto </h1>";
                                header('Refresh:3 Loguear.php');
                            endif;
                        else: //Si está mal la verificación, se hará lo siguiente:
                            $_SESSION['errorLogin'] = "Error de login"; //Asignamos la variable de sesión para el error (contraseña incorrecta)
                            echo "<h1> Contraseña incorrecta </h1>";
                            header('Refresh:3 Loguear.php');
                        endif;
            
                    } else {
            
                        echo "<h1> El correo ingresado es incorrecto o no exite ningún registro con los datos ingresados </h1>";
                        header('Refresh:5 Loguear.php');
            
                    }
        
                } else {
                    echo "<h1> Debe de ingresar datos en todos los campos </h1>";
                    header("Refresh:5 Loguear.php");
                }
            } else {
                header('Location: Loguear.php');
            }
            ?>
            
        </header>
    </body>
</html>

