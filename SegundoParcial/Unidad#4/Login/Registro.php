<?php

include './Conectar.php'; //Incluímos la conexión

if (isset($_POST['registrar'])) { //Preguntamos si existe la variable super global POST
    
    session_start(); //Inicio de la sesión para eliminar las variables de sesión de los datos vacíos y ya registrados
    if (isset($_SESSION['usuarioVacio'])) {
        unset($_SESSION['usuarioVacio']);
    }
    if (isset($_SESSION['contraseñaVacía'])) {
        unset($_SESSION['contraseñaVacía']);
    }
    if (isset($_SESSION['correoVacio'])) {
        unset($_SESSION['correoVacio']);
    }
    
    if (isset($_SESSION['usuarioOcupado'])) {
        unset($_SESSION['usuarioOcupado']);
    }
    if (isset($_SESSION['correoOcupado'])) {
        unset($_SESSION['correoOcupado']);
    }
    
    //Si no están vacíos los datos del formulario; se asignarán a las variables definidas,
    //caso contrario se llenarán por datos por defecto
    if (!empty($_POST['nombres'])) :
        $nombres = $_POST['nombres'];
    else:
        $nombres = "";
    endif;
    
    if (!empty($_POST['apellidos'])) :
        $apellidos = $_POST['apellidos'];
    else:
        $apellidos = "";
    endif;
    
    if (!empty($_POST['edad'])) :
        $edad = $_POST['edad'];
    else:
        $edad = 0;
    endif;
    
    if (!empty($_POST['fechaNacimiento'])) :
        $fechaNacimiento = $_POST['fechaNacimiento'];
    else:
        $fechaNacimiento = "0000-00-00";
    endif;
    
    if (!empty($_POST['nombreUsuario'])) :
        $nombreUsuario = $_POST['nombreUsuario'];
    else:
        $_SESSION['usuarioVacio'] = "El nombre de usuario no puede estar vacío."; //Variable de sesión en el caso de estar vacío
    endif;
    
    if (!empty($_POST['contraseña'])) :
        $contraseña = password_hash($_POST['contraseña'], PASSWORD_DEFAULT);
    else:
        $_SESSION['contraseñaVacía'] = "La contraseña no puede estar vacía."; //Variable de sesión en el caso de estar vacío
    endif;
    
    if (!empty($_POST['correo'])) :
        $correo = $_POST['correo'];
    else:
        $_SESSION['correoVacio'] = "El correo no puede estar vacío."; //Variable de sesión en el caso de estar vacío
    endif;
    
    if (isset($_SESSION['usuarioVacio']) || isset($_SESSION['contraseñaVacía']) || isset($_SESSION['correoVacio'])) {
        //Errores en el caso de vacíos
        echo "<h1 align='center'>Ups! Al parecer hubieron algunos errores</h1>";
        if (isset($_SESSION['usuarioVacio'])) :
            echo "&nbsp- " . $_SESSION['usuarioVacio'] . "<br> <br>";
        endif;
        if (isset($_SESSION['contraseñaVacía'])) :
            echo "&nbsp- " . $_SESSION['contraseñaVacía'] . "<br> <br>";
        endif;
        if (isset($_SESSION['correoVacio'])) :
            echo "&nbsp- " . $_SESSION['correoVacio'] . "<br> <br>";
        endif;
    } else {
        //Consulta para verificar si hay un usuario registrado con los mismos datos
        $query = "SELECT nombreUsuario, correo FROM usuario WHERE nombreUsuario = '$nombreUsuario' OR correo = '$correo'";
        $usuarioRegistrado =  mysqli_query($conexion, $query);
        if ($usuarioRegistrado && mysqli_num_rows($usuarioRegistrado) >= 1) {
            $usuarioOcupado = mysqli_fetch_assoc($usuarioRegistrado);
            if ($nombreUsuario == $usuarioOcupado['nombreUsuario']):
                $_SESSION['usuarioOcupado'] = "El nombre de usuario ya está ocupado por otra persona, intente ingresar otro nombre";
            endif;
            if ($correo == $usuarioOcupado['correo']):
                $_SESSION['correoOcupado'] = "El correo está siendo utilizado por otro usuario, pruebe con otro correo";
            endif;
        } else {
            //Consulta en el caso de que esté todo bien
            $sql = "INSERT INTO usuario (nombres, apellidos, edad, fechaNacimiento, nombreUsuario, contraseña, correo) "
                    . "VALUES ('$nombres', '$apellidos', $edad, '$fechaNacimiento', '$nombreUsuario', '$contraseña', '$correo');";
            $consulta = mysqli_query($conexion, $sql); //Ejecutando la consulta;
        }
    }
    
    ?>
    <html>
        <head>
            <title>Log-in de usuario</title>
            <link rel="stylesheet" type="text/css" href="Assets/Styles.css" />
        </head>
        <body>
            <header class="cabecera">
            <?php
            if (isset($_SESSION['usuarioOcupado'])) {
                echo $_SESSION['usuarioOcupado'] . "<br> <br>";
            }
            if (isset($_SESSION['correoOcupado'])) {
                echo $_SESSION['correoOcupado'] . "<br> <br>";
            }
            if (isset($consulta) && $consulta) {
                echo '<h1 align="center"> Se han ingresado los datos correctamente </h1>';
                header('Refresh:5 Loguear.php');
            } else {
                echo '<h1 align="center"> Error al ingresar </h1>';
                header('Refresh:10 Loguear.php');
            }
            ?>
            </header>
        </body>
    </html>
    <?php
    
} else {
    header('Location: Loguear.php');
}

