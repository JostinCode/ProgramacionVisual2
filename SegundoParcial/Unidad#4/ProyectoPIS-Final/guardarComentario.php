<?php

if (isset($_POST['enviarComentario'])) {
    
    require_once "./includes/conexion.php"; //Conexión y e inicio de sesión
    
    $comentario = !empty($_POST['comentario']) ? mysqli_real_escape_string($db, $_POST['comentario']) : false;
    $idProducto = !empty($_POST['idProducto']) ? $_POST['idProducto'] : false;
    $busqueda = $_POST['search'];
    
    /* Validar el comentario */
    if (!empty($comentario) && is_string($comentario) && strlen($comentario) <= 500) {
        $comentarioCorrecto = $comentario;
        $ejecutar = true;
    } else {
        $_SESSION['error']['comentario'] = "Comentario no válido";
        $ejecutar = false;
        if (strlen($comentario) > 500) {
            $_SESSION['error']['maxLongitud'] = "El comentario sólo debe de tener una cantidad menor o igual a 500 caracteres";
        }
    }
    
    if ($ejecutar) {
        $idUsuario = $_SESSION['usuario']['id'];
        $consulta = "SELECT * FROM comentarios WHERE idProducto = '$idProducto';";
        $select = mysqli_query($db, $consulta);
        if ($select && mysqli_num_rows($select) == 1) {
            $query = "UPDATE comentarios SET comentario = '$comentarioCorrecto', fechaComentario = CURDATE() WHERE idProducto = $idProducto;";
            $update = mysqli_query($db, $query);
            if ($update) :
                $_SESSION['comentarioCorrecto'] = "Se ha actualizado correctamente el comentario";
            else :
                $_SESSION['errorComentario'] = "Error al actualizar el comentario";
            endif;
        } elseif ($select && mysqli_num_rows($select) > 1) {
            $_SESSION['error']['comentarioDeMas'] = "¡Error! Comentario duplicado o de más";
        } else {
            $sql = "INSERT INTO comentarios VALUES (null, $idUsuario, $idProducto, '$comentarioCorrecto', CURDATE());";
            $insert = mysqli_query($db, $sql);
            if ($insert) :
                $_SESSION['comentarioCorrecto'] = "Se ha ingresado correctamente el comentario";
            else :
                $_SESSION['errorComentario'] = "Error al guardar el comentario";
            endif;
        }
    }
    
    $_SESSION['busqueda2'] = $busqueda;
    header('Location: buscar.php');
    
} else {
    
    header('Location: inicio.php');
    
}

