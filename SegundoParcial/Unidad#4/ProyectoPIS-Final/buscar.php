<?php require_once 'includes/cabeceraPrincipal.php'; ?>
<?php require_once 'includes/busqueda.php'; ?>
<?php require_once 'includes/cabeceraMenu.php'; ?>

<?php
$busqueda = isset($_POST['busqueda']) ? $_POST['busqueda'] : false;

if (isset($_POST['busqueda']) && isset($_SESSION['busqueda2'])) {
    unset($_SESSION['busqueda2']); //Eliminar búqueda anterior
}

if (isset($_SESSION['busqueda2']) && !empty($_SESSION['busqueda2'])) :
    $busqueda = $_SESSION['busqueda2'];
endif;

if (empty($busqueda)) {
    header("Location: inicio.php");
}
?>

<!-- CAJA PRINCIPAL -->
<div id="principal">
    
    <h1 align="center">Búsqueda de: <strong><?= $busqueda ?></strong></h1> <br> <br>
    
    <?php
    $productos = conseguirProductos($db, null, null, $busqueda);
    $numero = 0;
    $product = array();

    if ($productos && mysqli_num_rows($productos) >= 1):
        while ($producto = mysqli_fetch_assoc($productos)):
            $numero = $numero + 1;
            $product[$numero] = $producto;
            ?>
            <div id="producto">
                <article class="entrada">
                    <!-- <a href="<?php //entrada?id=<?=$producto['id_producto'] ?>"> -->
                    <h2 align="center" style="color: #45aaff;"><?= nl2br($producto['nombre']) ?></h2>
                    <span class="fecha"><?= $producto['categoria'] . ' | ' . $producto['tipoPago'] ?></span>
                    <p>
                        <?= nl2br(substr($producto['descripcion'], 0, 240)) . "..." ?>
                    </p>
                    <!-- </a> -->
                </article>
                <table border="" align="center">
                    <?php if (isset($producto['oferta']) && isset($producto['precioFinal'])) { ?>
                    <tr>
                        <td> <h3 align="center"><?= "Oferta: $" . $producto['oferta']; ?></h3> </td>
                    </tr>
                    <tr>
                        <td> <h3 align="center"><?= "Precio final: $" . $producto['precioFinal']; ?></h3> </td>
                    </tr>
                    <?php } elseif (isset($producto['oferta'])) { ?>
                    <tr>
                        <td> <h3 align="center"><?= "Oferta: $" . $producto['oferta']; ?></h3> </td>
                    </tr>
                    <?php } elseif (isset($producto['precioFinal'])) { ?>
                    <tr>
                        <td> <h3 align="center"><?= "Precio final: $" . $producto['precioFinal']; ?></h3> </td>
                    </tr>
                    <?php } ?>
                    <tr>
                        <td><img src="data:image/png;base64, <?php echo base64_encode($producto['imagen']) ?>" width="220px" height="240px"/></td>
                    </tr>
                </table>
            </div>
            <?php if (isset($_SESSION['usuario'])) { ?>
                <?php $comentarioObtenido = mostrarComentario($db, $_SESSION['usuario']['id'], $product[$numero]['id_producto']); ?>
                <div id="productoComent">
                    <h5>Comentarios: &nbsp;
                    <?php if (!empty($comentarioObtenido)) {
                        echo "Por " . $_SESSION['usuario']['nombreUsuario'] . " | " . $comentarioObtenido['fechaComentario'];
                    } ?>
                    </h5> &nbsp;
                    
                    <?php if (isset($_SESSION['error']['comentario'])) { ?>
                        <div class="mensajeComentario" id="errorComentario">
                            <?= $_SESSION['error']['comentario']; ?>
                        </div>
                        <?php unset($_SESSION['error']['comentario']) ?>
                    <?php } else if (isset($_SESSION['comentarioCorrecto'])) { ?>
                        <div class="mensajeComentario" id="comentarioCorrecto">
                            <?= $_SESSION['comentarioCorrecto']; ?>
                        </div>
                        <?php unset($_SESSION['comentarioCorrecto']) ?>
                    <?php } else if (isset($_SESSION['errorComentario'])) { ?>
                        <div class="mensajeComentario" id="errorComentario2">
                            <?= $_SESSION['errorComentario']; ?>
                        </div>
                        <?php unset($_SESSION['errorComentario']) ?>
                    <?php } else if (isset($_SESSION['error']['comentarioDeMas'])) { ?>
                        <div class="mensajeComentario" id="errorComentario3">
                            <?= $_SESSION['error']['comentarioDeMas']; ?>
                        </div>
                        <?php unset($_SESSION['error']['comentarioDeMas']) ?>
                    <?php } ?>
                    <br>
                    
                    <form action="guardarComentario.php" method="POST" enctype="multipart/form-data">
                        <textarea name="comentario"
                                placeholder="<?php if (isset($_SESSION['error']['maxLongitud'])) :
                                    echo $_SESSION['error']['maxLongitud'];
                                    unset($_SESSION['error']['maxLongitud']);
                                else :
                                    echo "Escriba algún comentario sobre el producto...";
                                endif; ?>"
                                style="width: 580px; height: 125px;
                                min-width: 580px; max-width: 580px;
                                min-height: 20px; max-height: 172px;
                                background: #f9f9f9;"
                                ><?php
                                if (isset($comentarioObtenido['comentario'])) :
                                    echo $comentarioObtenido['comentario'];
                                endif;
                                ?></textarea> <br>
                        <input type="hidden" name="idProducto" value="<?= $product[$numero]['id_producto']; ?>" />
                        <input type="hidden" name="search" value="<?= $busqueda; ?>" />
                        <input type="submit" name="enviarComentario" value="Comentar" />
                    </form>
                </div>
    <div style="clear: both; padding: 10px"></div>
            <?php } ?>
            <?php
        endwhile;
    else:
        ?>
        <div class="alerta">No hay productos de ninguna categoría referente a su búsqueda</div>
    <?php endif; ?>
</div>
<!--FIN PRINCIPAL-->

<?php require_once 'includes/pie.php'; ?>