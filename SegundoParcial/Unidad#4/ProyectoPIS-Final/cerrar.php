<?php

if (isset($_POST['cerrar'])) :
    
    session_start();
    if (isset($_SESSION['usuario'])) {
        session_destroy();
    }
    
endif;

header("Location: inicio.php");
