-- MySQL dump 10.13  Distrib 5.7.9, for Win64 (x86_64)
--
-- Host: localhost    Database: proyectopis
-- ------------------------------------------------------
-- Server version	5.7.11-log

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;
/*!40103 SET @OLD_TIME_ZONE=@@TIME_ZONE */;
/*!40103 SET TIME_ZONE='+00:00' */;
/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;

--
-- Table structure for table `usuarios`
--

DROP TABLE IF EXISTS `usuarios`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `usuarios` (
  `id` int(255) NOT NULL AUTO_INCREMENT,
  `nombres` varchar(100) CHARACTER SET utf8 DEFAULT NULL,
  `apellidos` varchar(100) CHARACTER SET utf8 DEFAULT NULL,
  `edad` int(3) DEFAULT NULL,
  `fechaNacimiento` date DEFAULT NULL,
  `nombreUsuario` varchar(100) COLLATE utf8_spanish_ci NOT NULL,
  `correo` varchar(255) CHARACTER SET utf8 NOT NULL,
  `contraseña` varchar(255) CHARACTER SET utf8 NOT NULL,
  `fechaSesion` date NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `uq_email` (`correo`)
) ENGINE=InnoDB AUTO_INCREMENT=8 DEFAULT CHARSET=utf8 COLLATE=utf8_spanish_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `usuarios`
--

LOCK TABLES `usuarios` WRITE;
/*!40000 ALTER TABLE `usuarios` DISABLE KEYS */;
INSERT INTO `usuarios` VALUES (1,'Cristopher José','Paredes Placencio',19,'2002-02-11','Cristof','Cristof@gmail.com','$2y$04$RSj2IiVEUIBUP83.pwsyuOx/99x7QbccEX7FvoxPrSgfX0nWZ5UOi','2021-09-26'),(3,'Nohely Josiany','Vera Jurado',20,'2001-01-26','Josy','Josiany@gmail.com','$2y$04$msF1Z/LMXooBd3v5fg3wu.AwXp8Q2x84Jm/ndxgdyW8EgUCAvrdgq','2021-09-26'),(4,'Pepe Alberto','Salazar Barzola',20,'2021-09-28','Pepito','AliasPepe@gmail.com','$2y$04$BmkPyLckw966kO9ihzB9v.1Nmtm56sxIATq.sLRv3R1mjuWyRpLnu','2021-09-28'),(5,'Martha','Moreno Díaz',20,'2021-09-30','martha2','martha@gmail.com','$2y$04$/mXmw1vAMVGZc8GZsIv2peZkgvszZpVlhmZiirxxGyOicOUkhv5wu','2021-09-30'),(6,'María Arelys','González Castro',18,'2002-11-11','Are','Are@gmail.com','$2y$04$ancjlRhK2ngt7OTNE4lEBOazqOwv4Pe1PhfB3CQfKCCfxxcGvAduK','2021-10-04'),(7,'Jostin Gerald','Vera Jurado',18,'2002-11-11','JostinVer','JostinVera@gmail.com','$2y$04$CsRfX4R4OfNby1c1f4xQTuoTNWgwxJwlaHICgKlqvle3PssbNhQfu','2021-10-05');
/*!40000 ALTER TABLE `usuarios` ENABLE KEYS */;
UNLOCK TABLES;
/*!40103 SET TIME_ZONE=@OLD_TIME_ZONE */;

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;

-- Dump completed on 2021-10-14 11:56:50
