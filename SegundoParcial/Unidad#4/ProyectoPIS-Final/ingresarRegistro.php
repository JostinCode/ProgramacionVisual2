<?php

if (isset($_POST['registrar'])) {

    // Conexión a la base de datos e inicio de sesión
    require_once 'includes/conexion.php';
    
    // Recorger los valores del formulario de registro
    $nombres = !empty($_POST['nombres']) ? mysqli_real_escape_string($db, $_POST['nombres']) : false;
    $apellidos = !empty($_POST['apellidos']) ? mysqli_real_escape_string($db, $_POST['apellidos']) : false;
    $edad = !empty($_POST['edad']) ? $_POST['edad'] : false;
    $fechaNacimiento = !empty($_POST['fechaNacimiento']) ? explode('-', $_POST['fechaNacimiento']) : false;
    $nombreUsuario = !empty($_POST['nombreUsuario']) ? mysqli_real_escape_string($db, $_POST['nombreUsuario']) : false;
    $correo = !empty($_POST['correo']) ? mysqli_real_escape_string($db, trim($_POST['correo'])) : false;
    $contraseña = !empty($_POST['contraseña']) ? mysqli_real_escape_string($db, $_POST['contraseña']) : false;

    // Array de errores
    $errores = array();

    // Validar los datos antes de guardarlos en la base de datos
    // Validar los nombres
    if (!empty($nombres) && !is_numeric($nombres) && !preg_match("/[0-9]/", $nombres)) {
        $nombresValidados = $nombres;
    } else {
        $errores['nombres'] = "Nombres no válidos";
    }

    // Validar los apellidos
    if (!empty($apellidos) && !is_numeric($apellidos) && !preg_match("/[0-9]/", $apellidos)) {
        $apellidosValidados = $apellidos;
    } else {
        $errores['apellidos'] = "Apellidos no válidos";
    }
    
    // Validar la edad
    if (!empty($edad) && is_numeric($edad) && preg_match("/[0-9]/", $edad) && $edad > 0 && $edad <=200) {
        $edadValidada = $edad;
    } else {
        $errores['edad'] = "Edad no válida";
    }
    
    // Validar la fecha de nacimiento
    if (!empty($fechaNacimiento) && count($fechaNacimiento) == 3 &&
            checkdate($fechaNacimiento[1], $fechaNacimiento[2], $fechaNacimiento[0])) {
        $fechaNacimiento = $_POST['fechaNacimiento'];
        $fechaNacimientoValidada = $fechaNacimiento;
    } else {
        $errores['fechaNacimiento'] = "Fecha de nacimiento no válida";
    }
    
    // Validar el nombre de usuario
    if (!empty($nombreUsuario)) {
        $nombreUsuarioValidado = $nombreUsuario;
    } else {
        $errores['nombreUsuario'] = "Usuario no válido";
    }
    
    // Validar el password
    if (!empty($contraseña)) {
        $contraseñaValidada = $contraseña;
    } else {
        $errores['contraseña'] = "Contraseña no válida"; //La contraseña está vacía
    }

    // Validar el email
    if (!empty($correo) && filter_var($correo, FILTER_VALIDATE_EMAIL)) {
        $correoValidado = $correo;
    } else {
        $errores['correo'] = "Email no válido";
    }

    if (count($errores) == 0) {
        
        // Cifrar la contraseña
        $passwordSeguro = password_hash($contraseñaValidada, PASSWORD_BCRYPT, ['cost' => 4]);

        // INSERTAR USUARIO EN LA TABLA USUARIOS DE LA BBDD
        $sql = "INSERT INTO usuarios VALUES(null, '$nombresValidados', '$apellidosValidados', '$edadValidada', "
                . "'$fechaNacimientoValidada', '$nombreUsuarioValidado', '$correoValidado', '$passwordSeguro', CURDATE());";
        $guardar = mysqli_query($db, $sql);

//		var_dump(mysqli_error($db));
//		die();

        if ($guardar) {
            $_SESSION['completado'] = "¡El registro se ha completado con éxito!";
        } else {
            $_SESSION['errores']['general'] = "¡Fallo al guardar el registro de usuario!";
        }
    } else {
        $_SESSION['errores'] = $errores;
    }
}

header('Location: registro.php');
