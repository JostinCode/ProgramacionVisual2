<?php session_start(); ?>

<!-- Login de usuario -->
<html>
    <head>
        <meta charset="utf-8" />
        <title>Company Shop</title>
        <link rel="stylesheet" type="text/css" href="./assets/css/style.css" />
        <link rel="icon" type="image/png" href="./assets/img/logo3.png" />
    </head>
    <body id="registro">
        <div id="Log_in">
            <form action="login.php" method="POST" enctype="multipart/form-data">
                <table>
                    <tr>
                        <td align="center"> <a href="registro.php">Registrarse</a> </td>
                        <td align="center" colspan="2"> <a href="inicio.php">Ir a inicio</a> </td>
                    </tr>
                    <tr> <td id="otroEspacio2" class="espacio4" align="center" colspan="3"><h1> Inicie sesión en el sistema </h1></td> </tr>
                    <tr> <td class="espacio4" align="center" colspan="3"><img src="./assets/img/usuario.png" width="200"/> </td> </tr>
                    <?php if (isset($_SESSION['usuario']['estado'])) : ?>
                    <tr> <td id="correcto" class="espacio3" align="center" colspan="3"><?= $_SESSION['usuario']['estado'] ?></td> </tr>
                    <?php unset($_SESSION['usuario']['estado']);
                    header('Refresh:5 inicio.php'); ?>
                    <?php elseif (isset($_SESSION['errorLogin'])) : ?>
                    <tr> <td id="errorLogin" class="espacio3" align="center" colspan="3"><?= $_SESSION['errorLogin'] ?></td> </tr>
                    <?php unset($_SESSION['errorLogin']);
                    header('Refresh:5 loguear.php'); ?>
                    <?php endif; ?>
                    <tr>
                        <td class="espacio">Nombre de usuario:</td>
                        <td><abbr title="Digite su nombre de usuario"> <input type="text" name="usuario" placeholder="Usuario" /> </abbr></td>
                        <td align="center"> &nbsp; <img src="./assets/img/iconoUser.png" align="center" width="28" /></td>
                    </tr>
                    <tr>
                        <td class="espacio">Password o contraseña: &nbsp; </td>
                        <td <?php if (isset($_SESSION['errorContraseña'])) { ?> class="errores" <?php } ?> >
                            <abbr title="Digite su contraseña">
                                <?php if (!isset($_SESSION['errorContraseña'])) { ?>
                                <input type="password" name="contraseña" placeholder="Contraseña" />
                                <?php } else { ?>
                                <input type="password" name="contraseña" placeholder="<?= $_SESSION['errorContraseña'] ?>" />
                                <?php unset($_SESSION['errorContraseña']); ?>
                                <?php } ?>
                            </abbr>
                        </td>
                        <td align="center"> &nbsp; <img src="./assets/img/iconoPassword.png" align="center" width="25" /></td>
                    </tr>
                    <tr>
                        <td class="espacio">Correro electrónico:</td>
                        <td><abbr title="Digite su correro o Email"> <input type="email" name="correo" placeholder="Correo" /> </abbr></td>
                        <td align="center"> &nbsp; <img src="./assets/img/iconoEmail.png" align="center" width="29" /></td>
                    </tr>
                    <tr>
                        <td class="espacio5" colspan="3" align="center"><input id="botonIniciarSesion" type="submit" name="iniciarSesion" value="Iniciar sesión" /></td>
                    </tr>
                </table>
            </form>
        </div>
    </body>
</html>

<?php

