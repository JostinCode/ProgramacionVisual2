<?php session_start(); ?>

<!-- Registro de usuario -->
<html>
    <head>
        <meta charset="utf-8" />
        <title>Company Shop</title>
        <link rel="stylesheet" type="text/css" href="./assets/css/style.css" />
        <link rel="icon" type="image/png" href="./assets/img/logo3.png" />
    </head>
    <body id="registro">
        <div id="Log_in">
            <form action="ingresarRegistro.php" method="POST" enctype="multipart/form-data">
                <table align="center" border="0px">
                    <tr>
                        <td align="center"> <a href="inicio.php">Ir a inicio</a> </td>
                        <td align="center"> <a href="loguear.php">Log-in</a> </td>
                    </tr>
                    <tr> <td id="otroEspacio" class="espacio4" align="center" colspan="2"><h1> Regístro de usuario </h1></td> </tr>
                    <tr>
                        <td class="espacio4" align="center" colspan="2"><img src="./assets/img/usuario.png" width="200" /></td>
                    </tr>
                    <?php if (isset($_SESSION['completado'])) { ?>
                    <tr>
                        <td id="completado" class="espacio3" align="center" colspan="2"><?= $_SESSION['completado'] ?></td>
                        <?php session_destroy();
                        header('Refresh:5 registro.php'); ?>
                    </tr>
                    <?php } elseif (isset($_SESSION['errores']['general'])) { ?>
                    <tr>
                        <td id="errorGeneral" class="espacio3" align="center" colspan="2"><?= $_SESSION['errores']['general'] ?></td>
                        <?php session_destroy();
                        header('Refresh:5 registro.php'); ?>
                    </tr>
                    <?php } ?>
                    <tr>
                        <td class="espacio3">Nombres:</td>
                        <?php if (!isset($_SESSION['errores']['nombres'])) : ?>
                            <td><input type="text" name="nombres" placeholder="Ingrese sus dos nombres" /></td>
                        <?php else: ?>
                            <td class="errores"><input type="text" name="nombres" placeholder="<?= $_SESSION['errores']['nombres']; ?>" /></td>
                            <?php unset($_SESSION['errores']['nombres']); ?>
                        <?php endif; ?>
                    </tr>
                    <tr>
                        <td class="espacio3">Apellidos:</td>
                        <?php if (!isset($_SESSION['errores']['apellidos'])) : ?>
                            <td><input type="text" name="apellidos" placeholder="Ingrese sus dos apellidos" /></td>
                        <?php else: ?>
                            <td class="errores"><input type="text" name="apellidos" placeholder="<?= $_SESSION['errores']['apellidos']; ?>" /></td>
                            <?php unset($_SESSION['errores']['apellidos']); ?>
                        <?php endif; ?>
                    </tr>
                    <tr>
                        <td class="espacio3">Edad:</td>
                        <?php if (!isset($_SESSION['errores']['edad'])) : ?>
                            <td><input type="number" name="edad" placeholder="Ingrese su edad" /></td>
                        <?php else: ?>
                            <td class="errores"><input type="number" name="edad" placeholder="<?= $_SESSION['errores']['edad']; ?>" /></td>
                            <?php unset($_SESSION['errores']['edad']); ?>
                        <?php endif; ?>
                    </tr>
                    <tr>
                        <td class="espacio3">Fecha de nacimiento:</td>
                        <?php if (!isset($_SESSION['errores']['fechaNacimiento'])) : ?>
                            <td><input type="date" name="fechaNacimiento" placeholder="" /></td>
                        <?php else: ?>
                            <td class="errores"><input type="date" name="fechaNacimiento" placeholder="" /></td>
                            <?php unset($_SESSION['errores']['fechaNacimiento']); ?>
                        <?php endif; ?>
                    </tr>
                    <tr>
                        <td class="espacio3">Nombre de usuario:</td>
                        <?php if (!isset($_SESSION['errores']['nombreUsuario'])) : ?>
                            <td><input type="text" name="nombreUsuario" placeholder="Ingrese su usuario" /></td>
                        <?php else: ?>
                            <td class="errores"><input type="text" name="nombreUsuario" placeholder="<?= $_SESSION['errores']['nombreUsuario']; ?>" /></td>
                            <?php unset($_SESSION['errores']['nombreUsuario']); ?>
                        <?php endif; ?>
                    </tr>
                    <tr>
                        <td class="espacio3">Password o contraseña: &nbsp; </td>
                        <?php if (!isset($_SESSION['errores']['contraseña'])) : ?>
                            <td><input type="password" name="contraseña" placeholder="Ingrese su contraseña" /></td>
                        <?php else: ?>
                            <td class="errores"><input type="password" name="contraseña" placeholder="<?= $_SESSION['errores']['contraseña']; ?>" /></td>
                            <?php unset($_SESSION['errores']['contraseña']); ?>
                        <?php endif; ?>
                    </tr>
                    <tr>
                        <td class="espacio3">Correro electrónico:</td>
                        <?php if (!isset($_SESSION['errores']['correo'])) : ?>
                            <td><input type="email" name="correo" placeholder="Ingrese su correo" /></td>
                        <?php else: ?>
                            <td class="errores"><input type="email" name="correo" placeholder="<?= $_SESSION['errores']['correo']; ?>" /></td>
                            <?php unset($_SESSION['errores']['correo']); ?>
                        <?php endif; ?>
                    </tr>
                    <tr>
                        <td class="espacio5" colspan="2" align="center">
                            <input id="botonRegistrar" type="submit" name="registrar" value="Registrar" />
                        </td>
                    </tr>
                </table>
            </form>
        </div>
    </body>
</html>

