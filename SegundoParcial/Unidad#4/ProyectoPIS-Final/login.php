<?php

// Recoger datos del formulario
if (isset($_POST['iniciarSesion'])) {

    // Iniciar la sesión y la conexión a la bd
    require_once 'includes/conexion.php';

    // Borrar error antiguo
//    if (isset($_SESSION['errorLogin'])) {
//        unset($_SESSION['errorLogin']); //Verificar bien el error
//    }

    // Recoger datos del formulario
    $user = !empty($_POST['usuario']) ? $_POST['usuario'] : false;
    $password = !empty($_POST['contraseña']) ? $_POST['contraseña'] : false;
    $email = !empty($_POST['correo']) ? trim($_POST['correo']) : false;

    // Consulta para comprobar las credenciales del usuario
    $sql = "SELECT * FROM usuarios WHERE correo = '$email' AND nombreUsuario = '$user'";
    $login = mysqli_query($db, $sql);

    if ($login && mysqli_num_rows($login) == 1) {
        $usuario = mysqli_fetch_assoc($login);

        // Comprobar la contraseña
        $verify = password_verify($password, $usuario['contraseña']);

        if ($verify) {
            // Utilizar una sesión para guardar los datos del usuario logueado
            $_SESSION['usuario'] = $usuario;
            $_SESSION['usuario']['estado'] = "Usuario logueado correctamente";
        } else {
            // Si algo falla enviar una sesión con el fallo
            $_SESSION['errorContraseña'] = "!Contraseña incorrecta!";
        }
    } else {
        // mensaje de error
        $_SESSION['errorLogin'] = "!Login incorrecto!";
    }
}

// Redirigir al inicio.php
header('Location: loguear.php');
